<?php

class Jadwal {
    private $mysqli;
    private $pgw;

    function __construct($conn, $pegawai = null){
        $this->mysqli = $conn;
        $this->pgw = $pegawai;
    }

    // MENGHITUNG JUMLAH DATA YANG ADA
    public function jumlah_jadwal(){
        $db = $this->mysqli->conn;
        $sql = "SELECT count(*) as jumlah FROM tbl_jadwal";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENGAMBIL DATA DARI DATABASE BERDASARKAN KODE_JADWAL
    public function tampil($KODE_JADWAL = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_jadwal j, tbl_pegawai p WHERE j.PRODUSER_NIP = p.NIP";
        if($KODE_JADWAL != null){
            $sql .= " AND j.KODE_JADWAL = '$KODE_JADWAL'";
        }
        $sql .= " ORDER BY j.CREATED_AT ASC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENGAMBIL DATA DARI DATABASE BERDASARKAN STATUS NYA
    public function tampil_jadwal_by_status($STATUS = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_jadwal j, tbl_pegawai p WHERE j.PRODUSER_NIP = p.NIP";
        if($STATUS != null){
            $sql .= " AND j.$STATUS";
        }
        $sql .= " ORDER BY j.CREATED_AT ASC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENGAMBIL DATA DARI DATABASE BERDASARKAN KOLOM TERTENTU
    public function tampil_filter($kolom, $value){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_jadwal";
        if($kolom != null){
            $sql .= " WHERE $kolom = '$value'";
        }
        $sql .= " ORDER BY CREATED_AT ASC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENAMBAHKAN DATA ACARA/JADWAL BARU KE DATABASE
    public function tambah($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO tbl_jadwal(NAMA_ACARA,PRODUSER_NIP,TANGGAL_MULAI,TANGGAL_SELESAI,LOKASI,JAM)";
        $sql .= "VALUES('".$data['NAMA_ACARA']."','".$data['PRODUSER_NIP']."','".$data['TANGGAL_MULAI']."','".$data['TANGGAL_SELESAI']."','".$data['LOKASI']."','".$data['JAM']."')";
        $query = $db->query($sql) or ($db->error);
        $result = false;
        if ($query) {
            $query = $db->query('SELECT LAST_INSERT_ID() as LAST_ID')->fetch_object();  // MENGAMBIL ID DARI JADWAL YAGN BARU DITAMBAHKAN
            $data['KODE_JADWAL'] = $query->LAST_ID;
            $assign = $this->penugasan($data, "assign");
            if ($assign) {
                // MEMBUKA TAB BARU UNTUK MENGIRIM EMAIL KE PEGAWAI DAN PRODUSER
                $this->createNotifiaksi($data['KODE_JADWAL']);
                $result = true;
            }
        }
        return $result;
    }
    // MENGHAPUS DATA DARI DATABASE BERDASARKAN KODE_JADWAL
    public function hapus($KODE_JADWAL){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_jadwal SET TANGGAL_MULAI = '0000-00-00', TANGGAL_SELESAI = '0000-00-00', STATUS = 'DELETED' WHERE KODE_JADWAL = '$KODE_JADWAL'";
        if($KODE_JADWAL == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
            if ($query) {
                $data['KODE_JADWAL'] = $KODE_JADWAL;
                $this->penugasan($data, 'delete');
            }
        }
        return $query;
    }
    // MENGUBAH DATA DARI DATABASE BERDASARKAN KODE_JADWAL
    public function ubah($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_jadwal SET NAMA_ACARA = '".$data['NAMA_ACARA']."', PRODUSER_NIP = '".$data['PRODUSER_NIP']."', LOKASI = '".$data['LOKASI']."', JAM = '".$data['JAM']."', ";
        $sql .= "TANGGAL_MULAI = '".$data['TANGGAL_MULAI']."', TANGGAL_SELESAI = '".$data['TANGGAL_SELESAI']."' WHERE KODE_JADWAL = '".$data['KODE_JADWAL']."'";
        if($data['KODE_JADWAL'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
            if ($query) {
                $new_petugas = [];
                $pegawai = $data['pegawai'];
                for( $i=0 ; $i< $data['TOTAL_PEGAWAI']; $i++) {
                    if (isset($pegawai[$i])) {
                        array_push($new_petugas, $pegawai[$i]);
                    }
                }
                $old_petugas = $data['LIST_PEGAWAI'];

                // MENGHAPUS PEGAWAI LAMA DAN MENAMBAHKAN PEGAWAI BARU
                for( $i=0 ; $i< count($old_petugas); $i++) {
                    if (in_array($old_petugas[$i], $new_petugas)) {
                        $sql = "UPDATE tbl_penugasan SET TANGGAL_SELESAI = '".$data['TANGGAL_SELESAI']."',JAM = '".$data['JAM']."' WHERE KODE_JADWAL = '".$data['KODE_JADWAL']."' AND NIP = '".$old_petugas[$i]."'";
                        $key = array_search($old_petugas[$i], $new_petugas);
                        unset($new_petugas[$key]);
                    } else {
                        $sql = "UPDATE tbl_penugasan SET TANGGAL_SELESAI = NOW() - INTERVAL 1 DAY WHERE KODE_JADWAL = '".$data['KODE_JADWAL']."' AND NIP = '".$old_petugas[$i]."'";
                    }
                    $query = $this->query($sql);
                }
                $new_petugas = array_values($new_petugas);
                $new_data['pegawai'] = $new_petugas;
                $new_data['TOTAL_PEGAWAI'] = count($new_petugas);
                $new_data['KODE_JADWAL'] = $data['KODE_JADWAL'];
                $new_data['TANGGAL_MULAI'] = $data['TANGGAL_MULAI'];
                $new_data['TANGGAL_SELESAI'] = $data['TANGGAL_SELESAI'];
                $new_data['JAM'] = $data['JAM'];
                $assign = $this->penugasan($new_data, "assign");    
                if ($assign) {
                    $this->createNotifiaksi($data['KODE_JADWAL']);
                    $query = true;
                }
            } else {
                $query = false;
            }
        }
        return $query;
    }

    // MENJALANKAN SQL QUERY BEBAS
    public function query($sql){
        $db = $this->mysqli->conn;
        if($sql == null) {
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    // MELAKUKAN ASSIGNMENT KE PEGAWAI SETELAH TUGAS DIBUAT / DIUBAH
    public function penugasan($data, $type) {
        $db = $this->mysqli->conn;
        if ($type == "assign") {
            $pegawai = $data['pegawai'];
            for( $i=0 ; $i< $data['TOTAL_PEGAWAI']; $i++) {
                if (isset($pegawai[$i])) {
                    $sql = "INSERT INTO tbl_penugasan(NIP,KODE_JADWAL,TANGGAL_MULAI,TANGGAL_SELESAI,JAM)";
                    $sql .= "VALUES('".$pegawai[$i]."','".$data['KODE_JADWAL']."','".$data['TANGGAL_MULAI']."','".$data['TANGGAL_SELESAI']."','".$data['JAM']."')";
                    $db->query($sql);
                }
            }
        } else if ($type == "delete") {
            if ($data['KODE_JADWAL'] == null) {
                return false;
            }
            $sql = "UPDATE tbl_penugasan SET TANGGAL_MULAI = '0000-00-00', TANGGAL_SELESAI = '0000-00-00' WHERE KODE_JADWAL = '".$data['KODE_JADWAL']."'";
            $db->query($sql);
        }
        return true;
    }
    public function createNotifiaksi($KODE_JADWAL) {
        // Instantiation and passing `true` enables exceptions
        $surat = file_get_contents('models/template_surat.html');
        $surat = str_replace('[kota]', 'Bandung', $surat);
        $tanggal_surat = date("d F Y");
        $surat = str_replace('[tanggal_surat]', $tanggal_surat, $surat);
        $surat = str_replace('[nama_atasan]', 'Muhammad Sanif', $surat);
    
        try {
            // KIRIM EMAIL UNTUK PRODUSER
            $getAcara = $this->tampil($KODE_JADWAL);
            $acara = $getAcara->fetch_assoc();
        
            // Content
            $surat = str_replace('[nama_acara]', $acara['NAMA_ACARA'], $surat);
            $surat = str_replace('[Nama_Pegawai]', $acara['NAMA'], $surat);
            $surat = str_replace('[NIP]', $acara['PRODUSER_NIP'], $surat);
            $surat = str_replace('[Jabatan]', 'PRODUSER', $surat);
            $surat = str_replace('[nama_produser]', $acara['NAMA'], $surat);
            $surat = str_replace('[tgl]', $acara['TANGGAL_MULAI'].' s/d '.$acara['TANGGAL_SELESAI'], $surat);
            $surat = str_replace('[Shift]', $acara['JAM'], $surat);
            $surat = str_replace('[lokasi]', $acara['LOKASI'], $surat);

            $pesan_singkat = "*Aplikasi Sistem Penjadwalan Kerabat Kerja dan Peralatan Berbasis IT*\t\n\t\n";
            $pesan_singkat .= "*-- Pemberitahuan Penugasan --*\t\n";
            $pesan_singkat .= "*Nama Acara :* ".$acara['NAMA_ACARA']." \t\n*Nama Pegawai :* ".$acara['NAMA']." \t\n";
            $pesan_singkat .= "*NIP :* ".$acara['PRODUSER_NIP']." \t\n*Jabatan :* PRODUSER \t\n";
            $pesan_singkat .= "*Nama Produser :* ".$acara['NAMA']." \t\n*Tanggal :* ".$acara['TANGGAL_MULAI'].' s/d '.$acara['TANGGAL_SELESAI']." \t\n";
            $pesan_singkat .= "*Shift :* ".$acara['JAM']." \t\n*Lokasi :* ".$acara['LOKASI']." \t\n";

            $sql = "INSERT INTO `tbl_notifikasi` (`penerima`, `pesan`, `pesan_html`) VALUES ('".$acara['PRODUSER_NIP']."', '$pesan_singkat', '$surat')";
            $this->pgw->query($sql);

            // KIRIM EMAIL UNTUK PEGAWAI
            $sql = "SELECT * FROM tbl_penugasan t, tbl_pegawai p WHERE t.KODE_JADWAL = ".$KODE_JADWAL." AND t.NIP = p.NIP";
            $getListPegawai = $this->pgw->query($sql);
            
            while($penerima = $getListPegawai->fetch_assoc()) {
                // Content
                $surat = str_replace('[nama_acara]', $acara['NAMA_ACARA'], $surat);
                $surat = str_replace('[Nama_Pegawai]', $penerima['NAMA'], $surat);
                $surat = str_replace('[NIP]', $penerima['NIP'], $surat);
                $surat = str_replace('[Jabatan]', $penerima['JABATAN'], $surat);
                $surat = str_replace('[nama_produser]', $acara['NAMA'], $surat);
                $surat = str_replace('[tgl]', $acara['TANGGAL_MULAI'].' s/d '.$acara['TANGGAL_SELESAI'], $surat);
                $surat = str_replace('[Shift]', $acara['JAM'], $surat);
                $surat = str_replace('[lokasi]', $acara['LOKASI'], $surat);
            
                $pesan_singkat = "*Aplikasi Sistem Penjadwalan Kerabat Kerja dan Peralatan Berbasis IT*\t\n\t\n";
                $pesan_singkat .= "*-- Pemberitahuan Penugasan --*\t\n";
                $pesan_singkat .= "*Nama Acara :* ".$acara['NAMA_ACARA']." \t\n*Nama Pegawai :* ".$penerima['NAMA']." \t\n";
                $pesan_singkat .= "*NIP :* ".$penerima['NIP']." \t\n*Jabatan :* ".$penerima['JABATAN']." \t\n";
                $pesan_singkat .= "*Nama Produser :* ".$acara['NAMA']." \t\n*Tanggal :* ".$acara['TANGGAL_MULAI'].' s/d '.$acara['TANGGAL_SELESAI']." \t\n";
                $pesan_singkat .= "*Shift :* ".$acara['JAM']." \t\n*Lokasi :* ".$acara['LOKASI']." \t\n";
            
                $sql = "INSERT INTO `tbl_notifikasi` (`penerima`, `pesan`, `pesan_html`) VALUES ('".$penerima['NIP']."', '$pesan_singkat', '$surat')";
                $this->pgw->query($sql);
            }
        } catch (Exception $e) {
            print_r($e);
        }
    }
}
?>