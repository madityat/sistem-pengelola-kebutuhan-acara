<?php

class Cronjob {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }

    // FUNCTION UNTUK MENGUBAH STATUS ACARA/JADWAL MENJADI SELESAI/FINISHED JIKA DATE(TANGGAL_SELESAI) SUDAH TERLEWAT
    public function cron_jadwal(){
        $db = $this->mysqli->conn;

        date_default_timezone_set('Asia/Jakarta');
        $time = strtotime(date('H:i:s'));
        $where = '';
        if (strtotime('00:00:01') <= $time) {
            $where .= "(DATE(TANGGAL_SELESAI) < DATE(NOW()) AND JAM = 'Tidak dalam shift') OR";
        }
        if (strtotime('12:00:01') <= $time) {
            $where .= "(DATE(TANGGAL_SELESAI) = DATE(NOW()) AND JAM = 'Shift 1 ( Pukul 08:00 - 12:00 )') OR";
        }
        if (strtotime('17:00:01') <= $time) {
            $where .= "(DATE(TANGGAL_SELESAI) = DATE(NOW()) AND JAM = 'Shift 2 ( Pukul 13:00 - 17:00 )') OR";
        }
        if (strtotime('21:00:01') <= $time) {
            $where .= "(DATE(TANGGAL_SELESAI) = DATE(NOW()) AND JAM = 'Shift 3 ( Pukul 18:00 - 21:00 )') OR";
        }

        $sql = "UPDATE tbl_jadwal SET STATUS = 'FINISHED' WHERE ( $where (DATE(TANGGAL_SELESAI) < DATE(NOW())) ) AND STATUS = 'RUNNING'";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
}
?>