<?php
class Penugasan {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }
    // MENGHITUNG JUMLAH DATA YANG ADA
    public function jumlah_penugasan(){
        $db = $this->mysqli->conn;
        $sql = "SELECT count(*) as jumlah FROM tbl_penugasan";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENGAMBIL DATA DARI DATABASE BERDASARKAN KODE_JADWAL
    public function tampil($KODE_JADWAL = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_penugasan j, tbl_pegawai p WHERE j.NIP = p.NIP";
        if($KODE_JADWAL != null){
            $sql .= " AND j.KODE_JADWAL = '$KODE_JADWAL'";
        }
        $sql .= " ORDER BY j.LAST_UPDATED DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENGAMBIL DATA TUGAS YANG SEDANG AKTIF DARI DATABASE BERDASARKAN KODE_JADWAL
    public function tampil_active($KODE_JADWAL = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_penugasan j, tbl_pegawai p WHERE j.NIP = p.NIP";
        if($KODE_JADWAL != null){
            $sql .= " AND j.KODE_JADWAL = '$KODE_JADWAL'";
        }
        $sql .= " ORDER BY j.LAST_UPDATED DESC";
        $query = $db->query($sql);
        $hasil = [];
        if ($query) {
            $sql = "SELECT * FROM tbl_jadwal WHERE KODE_JADWAL = '$KODE_JADWAL'";
            $jadwal = $this->query($sql)->fetch_object();
            while($data = $query->fetch_object()) {
                if ($data->TANGGAL_SELESAI == $jadwal->TANGGAL_SELESAI) {
                    array_push($hasil, $data);
                }
            }

        }
        return $hasil;
    }
    // MENGAMBIL DATA DARI DATABASE BERDASARKAN KOLOM TERTENTU
    public function tampil_filter($kolom, $value){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_penugasan";
        if($kolom != null){
            $sql .= " WHERE $kolom = '$value'";
        }
        $sql .= " ORDER BY LAST_UPDATE ASC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENAMBAHKAN DATA PENUGASAN BARU KE DATABASE
    public function tambah($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO tbl_penugasan(NIP,KODE_JADWAL,TANGGAL_MULAI,TANGGAL_SELESAI)";
        $sql .= "VALUES('".$data['NIP']."','".$data['KODE_JADWAL']."','".$data['TANGGAL_MULAI']."','".$data['TANGGAL_SELESAI']."')";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    // MENGHAPUS DATA DARI DATABASE BERDASARKAN ID
    public function hapus($ID){
        $db = $this->mysqli->conn;
        $sql = "DELETE FROM tbl_penugasan WHERE ID_PENUGASAN = '$ID'";
        if($ID == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    // MENGUBAH DATA DARI DATABASE
    public function ubah($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_penugasan SET TANGGAL_MULAI = '".$data['TANGGAL_MULAI']."', TANGGAL_SELESAI = '".$data['TANGGAL_SELESAI']."'";
        $sql .= " WHERE KODE_JADWAL = '".$data['KODE_JADWAL']."' AND NIP = '".$data['NIP']."'";
        if($data['KODE_JADWAL'] == null || $data['NIP'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    // MENJALANKAN SQL QUERY BEBAS
    public function query($sql){
        $db = $this->mysqli->conn;
        if($sql == null) {
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function upload_file($data, $file) {
        // ambil data file
        $namaFile = $file['FILE']['name'];
        $file_basename = substr($namaFile, 0, strripos($namaFile, '.')); // get file extention
        $file_ext = substr($namaFile, strripos($namaFile, '.')); // get file name
        $namaSementara = $file['FILE']['tmp_name'];
        $maxsize    = 227483956;
        if(($file['FILE']['size'] >= $maxsize) || ($file["FILE"]["size"] == 0)) {
            return 'too large';
        } else {
            // tentukan lokasi file akan dipindahkan
            $dirUpload = "assets/laporan/";
            $namaFile = @$_SESSION['login_user']->NIP."_".$data['ID_PENUGASAN'].$file_ext;
            // pindahkan file
            
            $terupload = move_uploaded_file($namaSementara, $dirUpload.$namaFile);
            $path = $dirUpload.$namaFile;

            $query = false;
            if ($terupload) {
                $db = $this->mysqli->conn;
                $ID_PENUGASAN = explode('-', $data['ID_PENUGASAN']);
                if ($ID_PENUGASAN[0] == 'PRODUSER') {
                    $sql = "UPDATE tbl_jadwal SET BUKTI_TUGAS = '".$path."' WHERE KODE_JADWAL = '".$ID_PENUGASAN[1]."'";
                } else {
                    $sql = "UPDATE tbl_penugasan SET BUKTI_TUGAS = '".$path."' WHERE ID_PENUGASAN = '".$ID_PENUGASAN[1]."'";
                }
                $query = $db->query($sql);
                if ($query) {
                    $query = true;
                } else {
                    unlink($path);
                }
            }
            return $query;
        }
    }
}
?>