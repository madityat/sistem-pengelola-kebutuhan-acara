<?php
    $resp = null;
    if($json = json_decode(file_get_contents("php://input"), true)) {
        $data = $json;
    } else {
        $data = $_POST;
    }
    if (count($data) > 0 && (@$data['phone'] === '6282219230359' || @$data['phone'] === '6281223122798' || @$data['fromMe'] === true)) {
        $resp =  implode(', ', array_map(
            function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
            $data,
            array_keys($data)
        ));
    }
    echo $resp;
?>