<ul class="nav navbar-nav side-nav">
    <li><a href="?page=dashboard_pegawai"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Peminjaman <b class="caret"></b></a>
        <ul class="dropdown-menu">
        <li><a href="?page=peminjaman_input"><i class="fa fa-edit"></i> Input Peminjaman</a></li>
        <li><a href="?page=barang_dipinjam_data"><i class="fa fa-table"></i> Barang Sedang Dipinjam</a></li>
        <li><a href="?page=pengembalian_input"><i class="fa fa-edit"></i> Input Pengembalian</a></li>
    </ul>
    </li>
    <li><a href="?page=pengajuan_input"><i class="glyphicon glyphicon-envelope"></i> Pengajuan</a></li>
</ul>
