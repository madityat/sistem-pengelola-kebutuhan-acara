<ul class="nav navbar-nav side-nav">
    <li><a href="?page=dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Kelola Barang <b class="caret"></b></a>
        <ul class="dropdown-menu">
        <li><a href="?page=barang_input">Input Barang</a></li>
        <li><a href="?page=barang_data">Data Barang</a></li>
        <li><a href="?page=barang_dipinjam_data">Barang Sedang Dipinjam</a></li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Kelola Peminjaman <b class="caret"></b></a>
        <ul class="dropdown-menu">
        <li><a href="?page=peminjaman_data">Data Peminjaman</a></li>
        </ul>
    </li>
    <li><a href="?page=pengajuan_data"><i class="glyphicon glyphicon-envelope"></i> Pengajuan</a></li>
</ul>
