<div class="row">
          <div class="col-lg-12">
            <h1>Input Peminjaman <small><?php echo ucfirst($_SESSION['login_as']); ?></small></h1>
            <ol class="breadcrumb">
              <li><a href="index.php"><i class="icon-dashboard"></i> Input Peminjaman</a></li>
              <li class="active"><i class="icon-file-alt"></i> Blank Page</li>
            </ol>
          </div>
        </div><!-- /.row -->
        <?php
          include "models/m_barang.php";
          include "models/m_peminjaman.php";
          include "./../sisfopen/models/m_jadwal.php";
          $pjm = new Peminjaman($connection);
          $brg = new Barang($connection);
          $jwl = new Jadwal($connection);
        
          if (isset($_POST['tambah'])) {
            $tambah = $pjm->tambah($_POST);
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Data Berhasil Ditambahkan.';
            if (!$tambah) {
              $alert = 'alert alert-danger';
              $message = '<strong>Fail!</strong> Gagal Menambahkan Data.';
            }
            echo "
              <div class='".$alert."'>
                ".$message."
              </div>
            ";
          }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="form-group">
                <label for="barang">Keperluan Untuk Acara</label>
                <select class="form-control" id="acara" name="acara" onChange="setTanggal(this.value)" required>
                  <?php
                    $NIP = $_SESSION['login_user']->NIP;
                    if (($_SESSION['login_as']) === 'admin') {
                      $tampil = $jwl->query("SELECT * FROM tbl_jadwal WHERE STATUS = 'RUNNING'");
                    } else if (($_SESSION['login_as']) === 'produser') {
                      $tampil = $jwl->query("SELECT * FROM tbl_jadwal WHERE PRODUSER_NIP = '$NIP' AND STATUS = 'RUNNING'");
                    } else {
                      $tampil = $jwl->query("SELECT * FROM tbl_penugasan p, tbl_jadwal j WHERE p.NIP = '$NIP' AND p.KODE_JADWAL = j.KODE_JADWAL AND j.STATUS = 'RUNNING'");
                    }
                      echo "<option disabled selected>Pilih Acara</option>";
                      while($data = $tampil->fetch_assoc()){
                  ?>
                        <option value="<?php echo $data['KODE_JADWAL'].':'.$data['TANGGAL_MULAI'].':'.$data['TANGGAL_SELESAI']; ?>">
                          <?php echo $data['NAMA_ACARA']; ?>
                        </option>
                  <?php
                    }
                  ?>
                </select>
                <input type="hidden" name="KODE_JADWAL" class="form-control" id="KODE_JADWAL" value="" readonly required>
              </div>
              <div class="form-group">
                <div>
                  <label class="control-label">Tanggal Peminjaman</label>
                </div>
                <div class="col-md-6">
                  <label class="control-label" for="TANGGAL_MULAI">Dari :</label>
                  <input type="date" name="TANGGAL_MULAI" class="form-control" id="TANGGAL_MULAI" readonly required>
                </div>
                <div class="col-md-6">
                <label class="control-label" for="TANGGAL_SELESAI">Sampai :</label>
                  <input type="date" name="TANGGAL_SELESAI" class="form-control" id="TANGGAL_SELESAI" readonly required>
                </div>
                <div class="col-md-12">&nbsp</div>
              </div>
              <div class="form-group">
                <div>
                  <label class="control-label">Data Peminjaman</label>
                </div>
                <div class="col-md-4">
                  <label class="control-label" for="nip">NIP Peminjam</label>
                  <input type="text" name="nip" class="form-control" id="nip" placeholder="NIP Peminjam" value="<?php echo $_SESSION['login_user']->NIP ?>" readonly>
                </div>
                <div class="col-md-4">
                  <label class="control-label" for="nama_peminjam">Nama Peminjam</label>
                  <input type="text" name="nama_peminjam" class="form-control" id="nama_peminjam" placeholder="Nama Peminjam" value="<?php echo $_SESSION['login_user']->NAMA ?>" readonly>
                </div>
                <div class="col-md-4">
                  <label>Divisi Kerja</label>
                  <select class="form-control" name="divisi_kerja">
                    <option value="Bagian Teknik">Bagian Teknik</option>
                    <option value="Bagian Program">Bagian Program</option>
                    <option value="Bagian Berita">Bagian Berita</option>
                  </select>
                </div>
                <div class="col-md-12">&nbsp</div>
              </div>
              <div class="form-group">
                <label class="control-label" for="tujuan_peminjaman">Tujuan Peminjaman</label>
                <textarea class="form-control" rows="5" id="tujuan_peminjaman" name="tujuan_peminjaman" placeholder="Tujuan Peminjaman" Required></textarea>
              </div>
              <div class="form-group">
                <div>
                  <label for="barang">Pilih Barang</label>
                </div>
                <div class="row">
                  <?php
                    $no = 0;
                    $tampil = $brg->tampil_filter('status_ketersediaan', 1);
                    if ($tampil->num_rows === 0) {
                    ?>
                      - Tidak Dapat Mengambil Data Barang
                    <?php
                    } else {
                      $last_jenis = '';
                      while($data = $tampil->fetch_assoc()) {
                        if ($data['jenis_barang'] !== $last_jenis && $no > 0) {
                          echo "<div class='col-md-12'><hr></div>";
                        }
                  ?>
                        <div class="col-md-4">
                            <div class="input-group-text">
                                <input type="checkbox" name="barang[<?php echo $no;?>]" id="barang[<?php echo $no;?>]" value="<?php echo $data['kd_barang'].':'.$data['nama_barang'].':'.$data['jenis_barang']; ?>">
                                <label for="barang[<?php echo $no;?>]" class="mb-0">
                                  <?php echo $data['jenis_barang'].' - '.$data['nama_barang'].' ('.$data['kd_barang'].')'; ?>
                                </label>
                            </div>
                        </div>
                  <?php
                        $last_jenis = $data['jenis_barang'];
                        $no++;
                      }
                    }
                  ?>
                </div>
              </div>
            </div>
            <!-- Button simpan -->
            <div id="tambah" class="modal-footer">
              <?php
                if ($tampil->num_rows > 0) {
                  ?>
                    <input type="submit" class="btn btn-success" name="tambah" value="TAMBAH">
                  <?php
                }
              ?>
            </div>
            </div>
        </form>
        <div class="">
            <div class="col-lg-12">
                <div class = "table-resposive">
                    <table class="table table-bordered table-hover table-striped">
                        <tr>                            
                        </tr>
                    </table>
                </div>
            </div>
        </div>

<script>
function setTanggal(value) {
  var data = value.split(':');
  document.getElementById('KODE_JADWAL').value = data[0];
  document.getElementById('TANGGAL_MULAI').value = data[1];
  document.getElementById('TANGGAL_SELESAI').value = data[2];
}
</script>