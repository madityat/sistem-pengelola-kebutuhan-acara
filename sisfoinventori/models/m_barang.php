<?php
class Barang {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }
    public function jumlah_dipinjam(){
        $db = $this->mysqli->conn;
        $sql = "SELECT count(*) as jumlah_pinjam FROM tbl_barang WHERE status_ketersediaan != 0";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function jumlah_barang(){
        $db = $this->mysqli->conn;
        $sql = "SELECT count(*) as jumlah FROM tbl_databarang";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function tampil($kd_barang = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_databarang";
        if($kd_barang != null){
            $sql .= " WHERE kd_barang = '$kd_barang'";
        }
        $sql .= " ORDER BY jenis_barang ASC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function tampil_filter($kolom, $value){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_databarang";
        if($kolom != null){
            $sql .= " WHERE $kolom = '$value'";
        }
        $sql .= " ORDER BY jenis_barang ASC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function tambah($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO tbl_databarang(kd_barang,nama_barang,jenis_barang,spesifikasi_barang,tanggal_pengadaan)";
        $sql .= "VALUES('".$data['kd_barang']."','".$data['nama_barang']."','".$data['jenis_barang']."','".$data['spesifikasi_barang']."','".$data['tanggal_pengadaan']."')";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function hapus($kd_barang){
        $db = $this->mysqli->conn;
        $sql = "DELETE FROM tbl_databarang WHERE kd_barang = '$kd_barang'";
        if($kd_barang == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function ubah($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_databarang SET nama_barang = '".$data['nama_barang']."', jenis_barang = '".$data['jenis_barang']."', ";
        $sql .= "spesifikasi_barang = '".$data['spesifikasi_barang']."', tanggal_pengadaan = '".$data['tanggal_pengadaan']."' WHERE kd_barang = '".$data['kd_barang']."'";
        if($data['kd_barang'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
}
?>