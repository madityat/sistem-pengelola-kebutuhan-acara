<?php
class Peminjaman {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }

    public function tampil($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_peminjaman";
        if($id != null){
            $sql .= " WHERE id = $id";
        }
        $sql .= " ORDER BY tanggal_mulai DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function tampil_filter($kolom, $value){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_peminjaman";
        if($kolom != null){
            $sql .= " WHERE $kolom = '$value'";
        }
        $sql .= " ORDER BY tanggal_mulai DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function tambah($data){
        $db = $this->mysqli->conn;
        foreach ($data['barang'] as $key => $barang) {
            $barang = explode(':',$barang);
            $kd_barang = $barang[0];
            $nama_barang = $barang[1];
            $jenis_barang = $barang[2];
            $sql = "INSERT INTO tbl_peminjaman(kd_barang,nama_barang,jenis_barang,kode_jadwal,tanggal_mulai,tanggal_selesai,nama_peminjam,nip,tujuan_peminjaman,divisi_kerja)";
            $sql .= "VALUES('".$kd_barang."','".$nama_barang."','".$jenis_barang."','".$data['KODE_JADWAL']."','".$data['TANGGAL_MULAI']."','".$data['TANGGAL_SELESAI']."','".$data['nama_peminjam']."','".$data['nip']."','".$data['tujuan_peminjaman']."','".$data['divisi_kerja']."')";
            $query = $db->query($sql) or ($db->error);
            if ($query) {
                $this->ubah_ketersediaan_barang($kd_barang, 0);
            }
        }
        return $query;
    }
    public function ubah_ketersediaan_barang($kd_barang, $value){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_databarang SET status_ketersediaan = $value WHERE kd_barang = '$kd_barang'";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
}
?>