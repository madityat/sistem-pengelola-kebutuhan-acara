<?php
class Pengembalian {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }
    public function tampil($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_pengembalian";
        if($id != null){
            $sql .= " WHERE id = $id";
        }
        $sql .= " ORDER BY tanggal_mulai DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function tampil_filter($kolom, $value){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_pengembalian";
        if($kolom != null){
            $sql .= " WHERE $kolom = '$value'";
        }
        $sql .= " ORDER BY tanggal_pengembalian DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function tambah($data){
        $db = $this->mysqli->conn;
        $barang = explode(':',$data['barang']);
        $id_peminjaman = $barang[0];
        $kd_barang = $barang[1];
        $nip = $barang[2];
        $query = false;

        if ($nip == $data['nip']) {
            $sql = "INSERT INTO tbl_pengembalian(id_peminjaman,tanggal_pengembalian,status_pengembalian)";
            $sql .= "VALUES('".$id_peminjaman."','".$_POST['tgl_pengembalian']."','".$_POST['status_pengembalian']."')";
            $query = $db->query($sql) or ($db->error);
            if ($query) {
                $this->ubah_ketersediaan_barang($kd_barang, 1);
            }
        }
        return $query;
    }
    public function ubah_ketersediaan_barang($kd_barang, $value){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_databarang SET status_ketersediaan = $value WHERE kd_barang = '$kd_barang'";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
}
?>