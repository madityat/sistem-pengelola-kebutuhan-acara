# Project Title

Sisem Pengelola Kebutuhan Acara / SIJAKA PERSIT

### Prerequisites

Sebelum dapat menggunakan aplikasi ada beberapa hal yang harus anda siapkan

```
XAMPP
Text Editor (notepad, notepad++, vs code, etc)
```

### Installing

Langkah-langkah :

- Install XAMPP dan gunakan konfigurasi default dari XAMPP
```
https://www.apachefriends.org/download.html
```
- Jalankan xampp, kemudian klik Start untuk Apache dan MySQL
- Download source code aplikasi di https://gitlab.com/telkomdev-tisnadinata/sistem-pengelola-kebutuhan-acara
- Ekstrak hasil download tadi ke dalam folder
```
 C://xampp/htdocs/<nama projek>
```
- Buka browser anda (Firefox, Chrome, etc)
- Pada address bar kemudian masuk ke link berikut
```
http://localhost/phpmyadmin/
atau
http://127.0.0.1/phpmyadmin/
```
- Masuk ke menu Database
- Pada bagian create database masukan nama db_sipeka, kemudian klik tombol Create
- Database anda akan tampil pada bagian sisi kiri dengan nama db_sipeka
- Klik nama database, kemudian klik menu Import
- Klik button Choose File
- Setelah muncul popup pencarian file masuk ke folder projek yang tadi anda simpan di C://xampp/htdocs/<nama projek>
- Pilih file db_sipeka.sql
- Pada tampilan phpMyAdmin klik tombol Go pada bagian bawah
- Database anda berhasil di buat


### Running Project
- Buka browser (Firefox, Chrome, etc)
- Pada address bar ketik
```
http://localhost/<nama projek>/
```
