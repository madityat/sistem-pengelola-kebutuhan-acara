<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SIJAKAPERSIT</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>STASIUN JAWA BARAT</strong></h1>
                        </div>
                    </div>
                    <?php
                        if (isset($_POST['login_inventori']) || isset($_POST['login_penugasan'])) {	// MENGECEK JIKA BUTTON LOGIN DITEKAN
							if ($_POST['username'] == 'atasan' && $_POST['password'] == 'atasan') {	// JIKA USERNAME DAN PASSWORD = ADMIN
								$_SESSION['login'] = true;	// SET LOGIN = TRUE AGAR TAHU USER SUDAH LOGIN
								$_SESSION['login_as'] = 'atasan';	// SET LOGIN SEBAGAI ADMIN
								header("Location: ./sisfopen/?page=lihat_jadwal_laporan");
								die();
							} else {
                                // MEMASUKAN FILE KONEKSI, DATABASE DAN M_PEGAWAI DARI FOLDER MODELS
								require_once('./config/koneksi.php');
								require_once('./config/database.php');
								require_once('./sisfopen/models/m_pegawai.php');
							
								$connection = new Database($host, $user, $pass, $database);	// CONNECT KE DTABASE
								$pgw = new Pegawai($connection);

								$login = $pgw->tampil($_POST['username']);	// MENGAMBIL DATA DARI TBL_PEGAWAI BERDASARKAN USERNAME/NIP
								if ($login && @$login->num_rows > 0) {	// MENGECEK JIKA USER DITEMUKAN
									$login = $login->fetch_object();	// MENGAMBIL DATA
									if ($login->PASSWORD == $_POST['password']) {	// JIKA PASSWORD SAMA/BENAR
										$_SESSION['login'] = true;
										$_SESSION['login_as'] = strtolower($login->JABATAN);
										$_SESSION['login_user'] = $login;
										if ($_SESSION['login_as'] == 'atasan') {
											$url = "./sisfopen?page=lihat_jadwal_laporan";
										} else if (isset($_POST['login_inventori'])) {
                                            $url = './sisfoinventori/';
                                        } else if (isset($_POST['login_penugasan'])) {
                                            if($_SESSION['login_as'] == 'admin') {
                                                $url = "./sisfopen?page=buat_jadwal";
                                            } else {
                                                $url = "./sisfopen?page=lihat_jadwal_saya";
                                            }
                                        }
                                        header("Location: ".$url);
										die();
									} else {
										echo '<div style="color: red;width:100%;text-align: center;">Password Salah</div>';
									}
								} else {
									echo '<div style="color: red;width:100%;text-align: center;">User Tidak Ditemukan</div>';
								}
							}
						}
					?>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                            		<p style="font-weight: bold;">Masukan Username dan Password untuk masuk ke dalam sistem inventori / sistem penugasan</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-key"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" name="form-login" action="" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="username">Username</label>
			                        	<input type="text" name="username" placeholder="Username..." class="form-username form-control" id="username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="password">Password</label>
			                        	<input type="password" name="password" placeholder="Password..." class="form-password form-control">
			                        </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <button type="submit" name="login_inventori" class="btn">Login Sistem Inventori</button>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="submit" name="login_penugasan" class="btn" style="background: cornflowerblue;">Login Sistem Penugasan</button>
                                        </div>
                                    </div>
                                </form>                            
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
        
    </body>
</html>